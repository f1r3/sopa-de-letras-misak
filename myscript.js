// Init values

var matriz = [
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
];

var matrizW = [
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
	['','','','','','','','','','','',''],
];

var palabras = [
	['hija','n','i','m','p','a','sr'], // hija
	['hijo','n','u','s','k','r','k','a','i'], // hijo
	['mamá','u','sr','i'], // mama
	['abuela','sh','u','r','a'], // abuela
	['abuelo','sh','u','r'], // abuelo
	['esposo','k','e','k'], // esposo
	['esposa','sr','a','i'], // esposa
	['hermano','n','u','n','e','k'], // hermano
	['hermana','n','u','ch','a','k'], // hermana
	['papa','m','Ɵ','s','k','a','i'], // papa
	['padres','m','Ɵ','s','k','a','l','Ɵ'], // padres
	['niños','u','r','e','k'], // niños
	['tia','k','u','ch','i','u','sr','i'], // tia
	['primo','l','e','n','u','n','e','k'], // primo
	['suegra','m','a','m','i','t','a'], // suegra
	['suegro','p','a','p','a'] // suegro
];

var abecedario = ['a','e','i','u','Ɵ','sr','sh','y','p','ts','l','k','t','m','n','s','tr','ch','ll','ñ','w','r'];

var tamFil = 12;
var tamCol = 12;
var nPalabras = 5;

console.log(matriz);
console.log(matriz[0][1]);

console.log(matriz);
console.log(palabras[1]);
console.log(palabras[1].length);

console.log(abecedario);

finalizo = false;
puestas = [];
misak = [];
gPos = 0;

// Seleccionar las palabras

var seleccionadas = [];

function inicializar() {
	matriz = [
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
	];

	matrizW = [
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
		['','','','','','','','','','','',''],
	];
	seleccionadas = [];
	puestas = [];
	misak = [];
	palabrasMostradas.textContent = '';
	finalizo = false;
}



function noEstaEnSeleccionadas(palabra) {
	for(i=0; i<seleccionadas.length; i++) {
		if(seleccionadas[i] === palabra) {
			return false;
		}
	}
	return true;
}


function seleccionarPalabras() {
	for (i=0; i<nPalabras; i++) {
		pos = Math.floor(Math.random()*palabras.length);
		while(!noEstaEnSeleccionadas(palabras[pos])) {
			pos = Math.floor(Math.random()*palabras.length);
		}
		seleccionadas[i] = palabras[pos];
	}
}

// funcion si la posición esta libre
function estaLibre(x,y) {
	if(matriz[x][y] === ''){
		return true;
	}
	return false;
}

// funcion si no se sobrepasan los limites de la matriz
function cabeEnMatriz(x, y, tPal, direccion) {
	switch(direccion) {
		// Arriba
		case 1:
			if ( x-tPal+1 < 0 )
				return false;
			return true;
			break;
		// Abajo
		case 5:
			if ( x+tPal > tamFil-1 )
				return false;
			return true;
			break;
		// Izquierda
		case 7:
			if ( y-tPal+1 < 0 )
				return false;
			return true;
			break;
		// Derecha
		case 3:
			if ( y+tPal-1 > tamCol-1 )
				return false;
			return true;
			break;
		// Derecha-Arriba
		case 2:
			if ( y+tPal-1 > tamCol-1 || x-tPal+1 < 0 )
				return false;
			return true;
			break;
		// Derecha-Abajo
		case 4:
			if ( y+tPal-1 > tamCol-1 || x+tPal > tamFil-1 )
				return false;
			return true;
			break;
		// Izquierda-Abajo
		case 6:
			if ( y-tPal+1 < 0 || x+tPal > tamFil-1 )
				return false;
			return true;
			break;
		// Izquierda-Arriba
		case 8:
			if( y-tPal+1 < 0 || x-tPal+1 < 0 )
				return false;
			return true;
			break;
	}
}

// funcion para saber si se puede insertar la palabra
function sePuedeInsertar(palabra, x, y, direccion) {
	puede = true;
	switch(direccion) {
		// Arriba
		case 1:
			for (i=1; i<palabra.length; i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				x--;
			}
			break;
		// Abajo
		case 5:
			for(i=1;i<palabra.length;i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				x++;
			}
			break;
		// Izquierda
		case 7:
			for(i=1;i<palabra.length;i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				y--;
			}
			break;
		// Derecha
		case 3:
			for(i=1;i<palabra.length;i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				y++;
			}
			break;
		// Derecha-Arriba
		case 2:
			for(i=1;i<palabra.length;i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				x--;
				y++;
			}
			break;
		// Derecha-Abajo
		case 4:
			for(i=1;i<palabra.length;i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				x++;
				y++;
			}
			break;
		// Izquierda-Abajo
		case 6:
			for(i=1;i<palabra.length;i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				x++;
				y--;
			}
			break;
		// Izquierda-Arriba
		case 8:
			for(i=1;i<palabra.length;i++) {
				if (matriz[x][y] !== '' && matriz[x][y] !== palabra[i])
					puede = false;
				x--;
				y--;
			}
			break;
	}
	return puede;
}

// insertar palabra en la matriz
function insertarPalabraMatriz(palabra, x, y, direccion) {
	switch(direccion) {
		// Arriba
		case 1:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				x--;
			}
			break;
		// Abajo
		case 5:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				x++;
			}
			break;
		// Izquierda
		case 7:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				y--;
			}
			break;
		// Derecha
		case 3:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				y++;
			}
			break;
		// Derecha-Arriba
		case 2:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				x--;
				y++;
			}
			break;
		// Derecha-Abajo
		case 4:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				x++;
				y++;
			}
			break;
		// Izquierda-Abajo
		case 6:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				x++;
				y--;
			}
			break;
		// Izquierda-Arriba
		case 8:
			for(i=1;i<palabra.length;i++) {
				matriz[x][y] = palabra[i];
				x--;
				y--;
			}
			break;
	}
}

// colocar palabras en la sopa de letras
function colocarPalabras(palabra) {
	largo = palabra.length;
	tolerancia = 0;
	console.log("aqui vamos")
	do {
		x = Math.floor((Math.random()*tamCol));
		y = Math.floor((Math.random()*tamFil));
		tolerancia++;
	} while (matriz[x][y] !== '' && matriz[x][y] !== palabra[1] && tolerancia < 20);
	console.log('X = ',x);
	console.log('Y = ',y);
	if (matriz[x][y] === '' || matriz[x][y] === palabra[1]) {
		var almacen = [];
		inserto = false;
		console.log('almacen = ',almacen);
		console.log('noInserto = ',inserto);

		while(almacen.length != 8 && !inserto) {
			direccion = Math.floor((Math.random()*8)+1);
			console.log('direccion = ', direccion);
			if(almacen.indexOf(direccion) === -1) {
				almacen.push(direccion);
				console.log('almacen = ',almacen);
				if (cabeEnMatriz(x, y, palabra.length, direccion) && sePuedeInsertar(palabra, x, y, direccion)) {
					insertarPalabraMatriz(palabra, x, y, direccion);
					inserto = true;
					puestas.push(palabra[0]);
					traduccion ='';
					console.log('revisar: ',palabra);
					for(itM=1; itM<palabra.length; itM++)
						traduccion= traduccion + '' + palabra[itM];
					console.log('traduccion: ',traduccion);
					misak.push(traduccion);
					console.log('misak: -> ',misak);
				}
			}
		}
	}
}

//mostrar las palabras que se deben buscar al jugar en la sopa de letras
var palabrasMostradas = document.querySelector("#pp");

function mostrarPalabras() {
	for (i=0; i<puestas.length; i++) {
		palabrasMostradas.textContent += ' [' + puestas[i] + ' : ' + misak[i] + '] ';
	}
}

// titulo de ganar
var tituloWin = document.querySelector("#won");


// Restart Game Button
//var restart = document.querySelector("#b");

// Grabs all the squares

var squares = document.querySelectorAll('td');


// mostrar la matriz en el html
function mostrarMatriz() {
	cont=0;
	f=0;
	c=0;
	for (itr=0; itr<squares.length; itr++) {
		squares[itr].textContent = 'h';
		if (c == 12) {
			c = 0;
			f++;
		}
		if (matriz[f][c] === '') {
			squares[itr].textContent = abecedario[Math.floor((Math.random()*abecedario.length))];
		}
		else
			squares[itr].textContent = matriz[f][c];
		c++;
	}
}

// Clear all the squares
function clearBoard() {
	for (var i=0; i<squares.length; i++) {
		squares[i].textContent = '';
		squares[i].style.backgroundColor = 'white';
	}
}

//restart.addEventListener('click', clearBoard);

// Check the square marker
/*
var cellOne = document.querySelector('#one')

cellOne.addEventListener('click', function(){
	if ( cellOne.textContent === '') {
		cellOne.textContent = 'X';
	} else if (cellOne.textContent === 'X') {
		cellOne.textContent = 'O';
	} else {
		cellOne.textContent = '';
	}
})
*/

function gano(){
	winner = true;
	for(itG=0; itG<matriz[0].length; itG++) {
		for(itW=0; itW<matriz.length; itW++) {
			if(matrizW[itG][itW] !==  matriz[itG][itW]){
				winner = false;
				itW = matriz.length;
				itG = matriz[0].length;
			}
		}
	}
	return winner;
}


function changeMarker(testeo) {
	console.log(this.id);
	posX = Math.floor(this.id/matriz[0].length);
	posY = this.id%matriz.length;
	if(!finalizo) {
		console.log('x = ', posX);
		console.log('y = ', posY);

		if (this.style.backgroundColor == 'white') {
			this.style.backgroundColor = "yellow";
			if(matriz[posX][posY]!=='') {
				matrizW[posX][posY] = matriz[posX][posY];
			} else {
				matrizW[posX][posY] = '~';
			}
		} else {
			this.style.backgroundColor = 'white';
			matrizW[posX][posY] = '';
		}
		if(gano() && tituloWin.textContent !== ''){
			console.log("ganaste");
			tituloWin.textContent = "Ganaste!";
			finalizo = true;
		}
		console.log(matrizW);
	}
}

// For loop to add event Listeners to all the squares y le coloca un id distinto a cada td
for (var i=0; i<squares.length; i++) {
	squares[i].id = i;
	squares[i].addEventListener('click', changeMarker);
}

//______________________________________________________________________



// Boton de Jugar
var jugar = document.querySelector("#j");

function iniciarJuego() {
	inicializar();
	clearBoard();
	seleccionarPalabras();
	tituloWin.textContent = 'Resuelve...';
	console.log('tamaño seleccionadas: ',seleccionadas.length);
	for(it=0; it<seleccionadas.length; it++) {
		colocarPalabras(seleccionadas[it]);
	}
	//llenarAleatoriamenteMatriz();
	console.log(seleccionadas);
	console.log(puestas);
	console.log(matriz);
	mostrarMatriz();
	mostrarPalabras();
}


jugar.addEventListener('click', iniciarJuego);

iniciarJuego();
